const form = document.getElementById('form')

function validateForm() {
    var name = document.forms["regform"]["name"].value;
    var telefon = document.forms["regform"]["telefon"].value;
    var obiective = document.getElementById("obiective");
    var serviciu = document.getElementById("serviciu")
    var info = document.getElementById("info");

    if (name == null || name == "") {
        alert("Introduceti numele");
        return false;
    } else if (telefon == null || telefon == "") {
        alert("Introduceti email-ul");
        return false;
    } else if (obiective.selectedIndex == 0) {
        alert("Selecteaza tipul de autovehicul");
        return false;
    } else if (serviciu.selectedIndex == 0) {
        alert("Selecteaza tipul de serviciu");
        return false;
    } else if (info.selectedIndex == 0) {
        alert("Selecteaza tipul de informatie");
        return false;

    }

}

var slideIndex = 1;
var timer = null;
showSlides(slideIndex);

function plusSlides(n) {
    clearTimeout(timer);
    showSlides(slideIndex += n);
}

function currentSlide(n) {
    clearTimeout(timer);
    showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");
    if (n == undefined) { n = ++slideIndex }
    if (n > slides.length) { slideIndex = 1 }
    if (n < 1) { slideIndex = slides.length }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " active";
    timer = setTimeout(showSlides, 8000);
}
/*function form no reset page*/
form.addEventListener('submit', function(e) {
    e.preventDefault()
    validateForm()
});

/* Toggle between showing and hiding the navigation menu links when the user clicks on the hamburger menu / bar icon*/
function myFunction() {
    var x = document.getElementById("myLinks");
    if (x.style.display === "block") {
        x.style.display = "none";
    } else {
        x.style.display = "block";
    }
}



function nightmode() {
    var element = document.body;
    element.classList.toggle("dark-mode");
    var button = document.getElementById('negru');
    var color = button.style.color;
    element.addEventListener('click', function() {
        // this function executes whenever the user clicks the button
        color = button.style.color = color === 'white' ? 'black' : 'white';
    })

}